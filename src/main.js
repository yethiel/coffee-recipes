import "./app.css";
import "tachyons/css/tachyons.min.css";
import App from "./App.svelte";

const app = new App({
    target: document.getElementById("app"),
});

export default app;
